import {
    FETCT_PRODUCT_INIT,
    FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCT_FAILURE
} from "./constants";
import { INIT, LOADING, SUCCESS, ERROR } from '../../utils/constant';

const initialState = {
    items: [],
    phase: INIT,
    error: null
};
export default function productReducer(state = initialState, action) {
    switch (action.type) {
      case FETCT_PRODUCT_INIT:
        return {
          ...state,
          phase: LOADING,
          error: null
        };
      case FETCH_PRODUCT_SUCCESS:
        return {
          ...state,
          phase: SUCCESS,
          items: action.payload.products
        };
      case FETCH_PRODUCT_FAILURE:
        return {
          ...state,
          phase: ERROR,
          error: action.payload.error,
          items: []
        };
      default:
        return state;
    }
  }
  