import fetch from '../../utils/fetch';
import {
    FETCT_PRODUCT_INIT,
    FETCH_PRODUCT_SUCCESS,
    FETCH_PRODUCT_FAILURE
} from "./constants";

// action types
export const pruductFetchInit = () => ({
  type: FETCT_PRODUCT_INIT
});

export const productFetchSuccess = products => ({
  type: FETCH_PRODUCT_SUCCESS,
  payload: { products }
});

export const productFetchFailure = error => ({
  type: FETCH_PRODUCT_FAILURE,
  payload: { error }
});

// actions
export function ApiFetch() {
  return dispatch => {
    dispatch(pruductFetchInit());
    return fetch("get-user-profile")
      .then(json => {
        dispatch(productFetchSuccess(json.user));
        // return json.user;
      })
      .catch(error => dispatch(productFetchFailure(error)));
  };
}