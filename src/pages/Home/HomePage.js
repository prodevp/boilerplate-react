import React from "react";
import { connect } from "react-redux";
import PropTypes from 'prop-types';
import { LOADING, ERROR } from '../../utils/constant';
import { ApiFetch } from "../../store/products/action";

class Home extends React.Component {
  static propTypes = {
    fetchProducts: PropTypes.func.isRequired,
    error: PropTypes.any,
    phase: PropTypes.string.isRequired,
    products: PropTypes.any,
  }

  componentDidMount() {
    const { fetchProducts } = this.props;
    fetchProducts();
  }
  
  render() {
    const { error, phase, products } = this.props;
    if (phase === ERROR) {
      return <div>{error.message}</div>;
    }
    if (phase === LOADING) {
      return <div>Loading...</div>;
    }

    return <h1>{products.uname}</h1>;
  }
}

const mapStateToProps = state => ({
  products: state.products.items,
  loading: state.products.loading,
  error: state.products.error,
  phase: state.products.phase,
});

const mapDispatchToProps = dispatch => ({
    fetchProducts: () => dispatch(ApiFetch()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
