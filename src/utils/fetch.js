import { API_URI } from "./constant";

const handleNetworkError = () => {
  // eslint-disable-next-line no-undef
  alert(
    'Internet connection error!!! \n\nPlease check your internet connection and try again',
  );
};

const handleHTTPErrors = res => {
  if (res.ok) {
    return res;
  }

  return res.json().then(err => {
    throw err;
  });
};

const fakeHandleHTTPErrors = res => {
  return res.json().then(err => {
    throw err;
  });
};

const defaultHeaders = {
  Accept: 'application/json',
  'Content-Type': 'application/json',
};

export default (url, options) => {
  let authAddedOptions = options;
  if (typeof options !== 'object') {
    authAddedOptions = {};
  }
  if (typeof authAddedOptions.headers !== 'object') {
    authAddedOptions.headers = {};
  }
  authAddedOptions.headers = {
    ...defaultHeaders,
    ...authAddedOptions.headers,
  };
//   fetch(url, authAddedOptions).then(
//     handleHTTPErrors,
//     handleNetworkError,
//   );
return fetch(API_URI+url)
.then(res => handleHTTPErrors(res))
.then(res => res.json())
.then(json => json )
.catch(error => handleNetworkError(error));
};