const INIT = 'INIT';
const LOADING = 'LOADING';
const SUCCESS = 'SUCCESS';
const ERROR = 'ERROR';
const CLEAR_STORES = 'CLEAR_STORES';
let API_URI;
const NETWORK_ERROR_MESSAGE = 'Network request failed';

if(process.env.NODE_ENV === "production"){
    API_URI = 'http://productionuri';
} else {
    API_URI = 'http://demo8679982.mockable.io/'
}

export {
    API_URI,
    INIT,
    LOADING,
    SUCCESS,
    ERROR,
    CLEAR_STORES,
    NETWORK_ERROR_MESSAGE
};