import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Home, Login } from './pages';

const App = () => (
    <Router>
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/about" component={Login} />
        <Route path="/productList" component={Home} />
      </Switch>
    </Router>
);

export default App;
